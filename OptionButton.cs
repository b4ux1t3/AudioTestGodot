using Godot;
using System;

public partial class OptionButton : Godot.OptionButton
{
	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		foreach (var s in AudioServer.GetOutputDeviceList())
		{
			AddItem(s);
		}
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(double delta)
	{
	}

	private void _on_item_selected(int i) {
		AudioServer.OutputDevice = GetItemText(i);
		CallDeferred(MethodName._setName);
	}
	
	private void _setName() => GetNode<Label>("../Output Device Label/Label").Text = AudioServer.OutputDevice;
}
